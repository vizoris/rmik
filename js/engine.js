
$(function() {

// MMENU
var menu = new MmenuLight(
  document.querySelector( '#mobile-menu' ),
  'all'
);

var navigator = menu.navigation({
  // selectedClass: 'Selected',
  // slidingSubmenus: true,
  // theme: 'dark',
   title: 'Меню'
});

var drawer = menu.offcanvas({
  // position: 'left'
});

//  Open the menu.
document.querySelector( 'a[href="#mobile-menu"]' )
  .addEventListener( 'click', evnt => {
    evnt.preventDefault();
    drawer.open();
});


// Выпадающее меню
if ($(window).width() >= 992) {
   $('.dropdown').hover(function() {
    $(this).children('ul').stop(true,true).fadeToggle();
   });

}else {
  $('.dropdown').click(function() {
    $(this).children('ul').stop(true,true).fadeToggle();
   })
}



$('.services-slider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed:5000,
    fade: true,
    arrows: true,
   
});


// Top portfolio
$('.slider-portfolio').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    centerMode: true,
    centerPadding: '0px',
    // autoplay: true,
    // autoplaySpeed:5000,
    fade: false,
    arrows: true,
    responsive: [{
        breakpoint: 768,
        settings: {
            slidesToShow: 1,
        }
        }
    ]
   
});



// Услуги подробнее на мобыльном
$('.services-item__text--mobile').click(function(event) {
    $(this).parent().find('p').fadeToggle();
    event.preventDefault();
    this.textContent = this.textContent === 'Читать подробнее' ? 'Скрыть' : 'Читать подробнее';
});


// Подкатегории подробнее на мобыльном
$('.tags .btn-main__transparent').click(function(event) {
    $(this).toggleClass('active');
    $(this).parent().find('.tags-list').fadeToggle();
    event.preventDefault();
    this.textContent = this.textContent === 'Посмотреть подкатегории' ? 'Скрыть подкатегории' : 'Посмотреть подкатегории';
})


// Отзывы подробнее на мобыльном
$('.reviews-item__body--more').click(function(event) {
    $(this).parent().find('.reviews-item__body').toggleClass('active');
    event.preventDefault();
    this.textContent = this.textContent === 'Развернуть' ? 'Свернуть' : 'Развернуть';
})


 // Стилизация селектов
$('select').styler();


$('.cart-slider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed:5000,
    fade: true,
    arrows: true,
   
});


// FansyBox
 $('.fancybox').fancybox({});



// Меню в футере на мобильном
if ($(window).width() <= 768) {
  $('.footer-links__item .h5').click(function() {
    $(this).next('ul').fadeToggle();
    $(this).toggleClass('active');
  })

}





})